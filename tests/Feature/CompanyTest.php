<?php

namespace Tests\Feature;

use App\Company;
use App\Events\CompanyCreated;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyTest extends TestCase
{
    use DatabaseMigrations;

    protected $company;
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->company = factory(Company::class)->create();
        $this->user = factory(User::class)->create(['email' => config('admin.email')]);

    }

    public function test_user_can_see_companies_index_page()
    {
        $this->actingAs($this->user);

        $response = $this->get(route('companies.index'));

        $response->assertViewIs('company.index');
        $response->assertViewHasAll(['companies']);
        $response->assertStatus(200);
    }

    public function test_user_can_see_specific_company_page()
    {
        $this->actingAs($this->user);

        $response = $this->get(route('companies.show', $this->company->id));

        $response->assertViewIs('company.show');
        $response->assertViewHasAll(['company']);
        $response->assertSee($this->company->name);
        $response->assertStatus(200);
    }

    public function test_user_can_see_specific_company_edit_page()
    {
        $this->actingAs($this->user);

        $response = $this->get(route('companies.edit', $this->company->id));

        $response->assertViewIs('company.edit');
        $response->assertViewHasAll(['company']);
        $response->assertSee($this->company->name);
        $response->assertStatus(200);
    }

    public function test_user_can_delete_company()
    {
        $this->actingAs($this->user);

        $response = $this->delete(route('companies.destroy', $this->company->id));

        $response->assertSessionHas('success', __('responses.successfully-deleted'));
        $response->assertRedirect();
        $response->assertStatus(302);
    }

    public function test_user_can_create_company()
    {
        $this->actingAs($this->user);
        $data = factory(Company::class,'withoutLogo')->make()->toArray();
        $this->expectsEvents([CompanyCreated::class]);

        $response = $this->post(route('companies.store'), $data);

        $this->assertDatabaseHas('companies', [
            'email' => $data['email']
        ]);

        $response->assertRedirect(route('companies.index'));
        $response->assertSessionHas('success', __('responses.successfully-created'));
        $response->assertStatus(302);
    }

    public function test_user_can_update_company()
    {
        $this->actingAs($this->user);
        $data = factory(Company::class,'withoutLogo')->make()->toArray();

        $response = $this->patch(route('companies.update', $this->company->id), $data);

        $response->assertRedirect(route('companies.edit', $this->company->id));
        $response->assertSessionHas('success', __('responses.successfully-updated'));
        $response->assertStatus(302);
    }


}
