<?php

namespace Tests\Unit;

use App\Company;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    use DatabaseMigrations;

    protected $company;

    public function setUp()
    {
        parent::setUp();
    }

    public function test_company_has_employees()
    {
        $this->company = factory(Company::class)->create();

        $this->assertInstanceOf(Collection::class, $this->company->employees);
    }
}
