<?php

namespace Tests\Unit;

use App\Company;
use App\Employee;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTest extends TestCase
{
    use DatabaseMigrations;

    protected $employee;

    public function setUp()
    {
        parent::setUp();

        $this->employee = factory(Employee::class)->create();
    }

    public function test_employee_belongs_to_company()
    {
        $this->assertInstanceOf(Company::class, $this->employee->company);
    }
}
