<?php

use Faker\Generator as Faker;

$factory->define(\App\Company::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'website' => $faker->unique()->url,
        'logo' => $faker->unique()->imageUrl()
    ];
});


$factory->defineAs(\App\Company::class, 'withoutLogo', function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'website' => $faker->unique()->url,
     ];
});

