<?php
return [
    'companies-index-title' => 'Companies',
    'employees-index-title' => 'Employees',
    'name' => 'Name',
    'email' => 'Email',
    'title' => 'Title',
    'create' => 'Create',
    'actions' => 'Actions',
    'language' => 'Язык',
    'logo' => 'Лого'

];
