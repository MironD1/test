<?php

return [
    'successfully-created' => 'Успешно создан!',
    'successfully-deleted' => 'Успешно удален!',
    'successfully-updated' => 'Успешно обновлен!'
];
