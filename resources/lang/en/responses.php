<?php

return [
    'successfully-created' => 'Successfully created!',
    'successfully-deleted' => 'Successfully deleted!',
    'successfully-updated' => 'Successfully updated!'
];
