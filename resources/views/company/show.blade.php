@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <h5>@lang('web.name')</h5>
            <p>{{$company->name}}</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <h5>@lang('web.email')</h5>
            <p>{{$company->email}}</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <h5>@lang('web.website')</h5>
            <p>{{$company->website}}</p>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <h5>@lang('web.logo')</h5>
            <img src="{{asset($company->logo)}}" alt="{{$company->logo}}" width="200" height="200">
        </div>
    </div>
</div>
@endsection
