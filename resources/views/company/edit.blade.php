@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <form method="POST" action="{{ route('companies.update', $company->id) }}"
                          enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">
                                Name
                            </label>

                            <input id="name" type="text" class="form-control" name="name"
                                   value="{{ old('name', $company->name)}}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">
                                Email
                            </label>

                            <input id="api_seller_id" type="text" class="form-control" name="email"
                                   value="{{ old('email', $company->email)}}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="website" class="control-label">
                                Website
                            </label>

                            <input id="website" type="text" class="form-control" name="website"
                                   value="{{ old('website', $company->website)}}" required>

                            @if ($errors->has('website'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('website') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                            <label for="logo" class="control-label">
                                Logo
                            </label>

                            <input type="file" name="logo" id="logo">

                            @if ($errors->has('logo'))
                                <span class="help-block">
                                            <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-sm btn-primary pull-left m-t-n-xs">Save</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
