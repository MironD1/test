@extends('layouts.app')
@section('content')
    <div class="container">
        <a href="{{route('companies.create')}}"  class="btn btn-success mb-2">@lang('Create')</a>

        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Logo</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)
                <tr>
                    <td>{{$company->id}}</td>
                    <td>{{$company->name}}</td>
                    <td>{{$company->email}}</td>
                    <td><img src="{{$company->logo}}" alt="logo" width="100" height="100"></td>
                    <td>
                        <a href="{{route('companies.show', $company->id)}}" class="view" title="View" data-toggle="tooltip"><i
                                    class="material-icons">&#xE417;</i></a>
                        <a href="{{route('companies.edit', $company->id)}}" class="edit" title="Edit" data-toggle="tooltip"><i
                                    class="material-icons">&#xE254;</i></a>
                        <form method="POST" action="{{route('companies.destroy', $company->id)}}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$companies->links()}}
    </div>
@endsection
