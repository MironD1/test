@if (\Session::has('success'))
    <div class="container">
        <div class="row">
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('success') !!}</li>
                </ul>
            </div>
        </div>
    </div>
@endif
