<?php

namespace App\Listeners;

use App\Notifications\NewCompanyCreated;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CompanyEventSubscriber
{
    public function notifyAdmin($event)
    {
        $companyId = $event->company->id;

        User::admin()->first()->notify(new NewCompanyCreated($companyId));
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Events\CompanyCreated',
            'App\Listeners\CompanyEventSubscriber@notifyAdmin'
        );
    }
}
