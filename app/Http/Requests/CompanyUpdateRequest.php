<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->company;

        return [
            'name' => ['required'],
            'email' => ['email','unique:companies,email,' . $id],
            'logo' => ['image', 'dimensions:min_width=100,min_height=100', 'unique:companies,logo'],
            'website' => ['url','unique:companies,website,' . $id]
        ];
    }
}
