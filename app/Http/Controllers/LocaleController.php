<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocaleController extends Controller
{
    public function setLocale(Request $request)
    {
        $locale = $request->locale;

        if (in_array($locale, \Illuminate\Support\Facades\Config::get('app.locales'))) {
            \Illuminate\Support\Facades\Session::put('locale', $locale);
        }

        return redirect()->back();
    }
}
