<?php

namespace App\Repositories;

use App\Company;
use App\Events\CompanyCreated;
use App\Repositories\Interfaces\RepositoryInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class CompanyRepository implements RepositoryInterface
{
    public function all()
    {
        return Company::paginate(10);
    }

    public function find($id)
    {
        return Company::find($id);
    }

    public function create(array $attributes)
    {
        if(isset($attributes['logo'])) {
            $attributes['logo'] = $this->saveLogoToStorage($attributes['logo']);
        }

        $company = Company::create($attributes);

        event(new CompanyCreated($company));

        return $company;
    }

    public function update(array $attributes, $id)
    {
        if (isset($attributes['logo'])) {
            $attributes['logo'] = $this->saveLogoToStorage($attributes['logo']);
        }

        return Company::where('id', $id)->update($attributes);
    }

    public function destroy($id)
    {
        return Company::destroy($id);
    }

    /**
     * @return string (filepath)
     */
    public function saveLogoToStorage(UploadedFile $file): string
    {
        $extension = $file->getClientOriginalExtension();
        $filename = 'profile-logo-' . time() . '.' . $extension;
        $path = $file->storeAs('logos', $filename, ['disk' => 'public']);

        return 'storage/' . $path;
    }


}
