<?php

namespace App\Repositories\Interfaces;


interface RepositoryInterface
{
    public function all();
    public function find(int $id);
    public function create(array $attributes);
    public function update(array $attributes, int $id);
    public function destroy(int $id);
}
